package com.example.demo.util;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.model.Details;

/**
 * @author Narendra The puspose of this class is to send the data from this
 *         application to the rabbit server
 */
@Component
public class RabbitSender {
	/*This is rabbittemplate in built class injected to send the data*/
	@Autowired
	RabbitTemplate rabbitTemplate;
    
	/**
	 * @param details
	 * this method taking details as method argument
	 * this method return void
	 */
	public void sendData(Details details) {
		rabbitTemplate.convertAndSend(details);
	}

}
